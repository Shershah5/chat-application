package aChatProgramAudio.copy;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class Client implements Runnable {

	public static void main(String args[]) throws Exception {
		String host = "127.0.0.1";

		DatagramSocket socket = new DatagramSocket();
		
		//handles the receiving part for every client (incoming packets to clients)
		MessageReceiver r = new MessageReceiver(socket);
		
		Client s = new Client(socket, host);
		Thread rt = new Thread(r);
		Thread st = new Thread(s);
		rt.start();
		st.start();
	}
	
	

	public final static int PORT = 7331;
	private DatagramSocket sock;
	private String hostname;

	Client(DatagramSocket s, String h) {
		sock = s;
		hostname = h;
	}

	//sending clients socket to server
	private void sendMessage(String s) throws Exception {
		//getting bytes from message
		byte buf[] = s.getBytes();
		
		//getting hostname from server
		InetAddress address = InetAddress.getByName(hostname);
		
		//setting up packet
		DatagramPacket packet = new DatagramPacket(buf, buf.length, address, PORT);
		
		//sending packet to server
		sock.send(packet);
	}

	public void run() {
		//connected boolean is used to send a greetings message once for every new client that has joined
		boolean connected = false;
		
		do {
			try {
				sendMessage("GREETINGS");
				connected = true;
			} catch (Exception e) {

			}
		} while (!connected);
		
		//reads from the console
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		
		while (true) {
			try {
				while (!in.ready()) {
					Thread.sleep(100);
				}
				
				//sends message from console to server
				sendMessage(in.readLine());
			} catch (Exception e) {
				System.err.println(e);
			}
		}
	}
}

//this class handles receiving part of clients
class MessageReceiver implements Runnable {
	DatagramSocket sock;
	byte buf[];

	MessageReceiver(DatagramSocket s) {
		sock = s;
		buf = new byte[1024];
	}

	public void run() {
		while (true) {
			try {
				DatagramPacket packet = new DatagramPacket(buf, buf.length);
				sock.receive(packet);
				String received = new String(packet.getData(), 0, packet.getLength());
				System.out.println(received);
			} catch (Exception e) {
				System.err.println(e);
			}
		}
	}
}
