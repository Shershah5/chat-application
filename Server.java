package aChatProgramAudio.copy;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;


public class Server extends Thread{

	public final static int PORT = 7331;
	private final static int BUFFER = 1024;

	private DatagramSocket socket;
	private ArrayList<InetAddress> clientAddresses;
	private ArrayList<Integer> clientPorts;
	private HashSet<String> existingClients;

	public Server() throws IOException {
		socket = new DatagramSocket(PORT);
		System.out.println("[SERVER] UDP server successfully launched on port " + PORT);
		
		clientAddresses = new ArrayList();
		clientPorts = new ArrayList();
		existingClients = new HashSet();
	}

	public void run() {
		byte[] buf = new byte[BUFFER];
		while (true) {
			try {
				//resets buffer so only new messages get displayed
				Arrays.fill(buf, (byte) 0);
				
				DatagramPacket packet = new DatagramPacket(buf, buf.length);
				socket.receive(packet);
				
				System.out.println("New client has connected.");
				
				//getData() from packet
				String content = new String(buf, buf.length);

				InetAddress clientAddress = packet.getAddress();
				int clientPort = packet.getPort();

				String id = clientAddress.toString() + "," + clientPort;
				if (!existingClients.contains(id)) {
					existingClients.add(id);
					clientPorts.add(clientPort);
					clientAddresses.add(clientAddress);
				}

				System.out.println(id + " : " + content);
				byte[] data = (id + " : " + content).getBytes();
				for (int i = 0; i < clientAddresses.size(); i++) {
					InetAddress cl = clientAddresses.get(i);
					int cp = clientPorts.get(i);
					packet = new DatagramPacket(data, data.length, cl, cp);
					socket.send(packet);
				}
			} catch (Exception e) {
				System.err.println(e);
			}
		}
	}

	public static void main(String args[]) throws Exception {
		Server s = new Server();
		s.start();
	}
}
